var structarctic_1_1easy_1_1_key_state =
[
    [ "Init", "structarctic_1_1easy_1_1_key_state.html#afe69e13080edc73e625a629f729e367d", null ],
    [ "IsDown", "structarctic_1_1easy_1_1_key_state.html#a42a3b47960e56227086a33b16bac4e64", null ],
    [ "OnShowFrame", "structarctic_1_1easy_1_1_key_state.html#af1a28801d37c08ef35adb3159e7b7b4b", null ],
    [ "OnStateChange", "structarctic_1_1easy_1_1_key_state.html#a5aa6893cc194e0e4cb496a96a31ef20d", null ],
    [ "WasPressed", "structarctic_1_1easy_1_1_key_state.html#a62b9f8b584d02cc92ed946bc2db969b5", null ],
    [ "WasReleased", "structarctic_1_1easy_1_1_key_state.html#a787ab8be4dea4d6cfa16104ec2af22c8", null ],
    [ "current_state_is_down", "structarctic_1_1easy_1_1_key_state.html#acfcd0548173219b3e5e25bcf99e116b2", null ],
    [ "previous_state_is_down", "structarctic_1_1easy_1_1_key_state.html#af03c9c88e837d23e10ece48945658f24", null ],
    [ "was_pressed_this_frame", "structarctic_1_1easy_1_1_key_state.html#a935b6c08d67ed6d41008a41a1434cad5", null ],
    [ "was_released_this_frame", "structarctic_1_1easy_1_1_key_state.html#a6c500ee56d05aa22d296cf0160033c6b", null ]
];