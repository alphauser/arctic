var structmz__stream__s =
[
    [ "adler", "structmz__stream__s.html#a24c6cf42b5b6a655f4664dd15203dce7", null ],
    [ "avail_in", "structmz__stream__s.html#aafcd4c220622ede6d54015f1fbdadd9e", null ],
    [ "avail_out", "structmz__stream__s.html#a9092fed61f7be520fb1bbcf152905ee8", null ],
    [ "data_type", "structmz__stream__s.html#a3e1503f72464a3b7b40d1df232ba9f43", null ],
    [ "msg", "structmz__stream__s.html#ad4095b25455e382787dc06d20157e05f", null ],
    [ "next_in", "structmz__stream__s.html#aa75ed42cc83b0f74d7fcce6299439307", null ],
    [ "next_out", "structmz__stream__s.html#a2c569383efce3b9d9e7c02e3e03941bc", null ],
    [ "opaque", "structmz__stream__s.html#a31c21928246598992a5cce6d20ae7e78", null ],
    [ "reserved", "structmz__stream__s.html#ae4fc708fffee7b10a4586964401613fb", null ],
    [ "state", "structmz__stream__s.html#a935cdb239d37161b5a5e6eda1e047df6", null ],
    [ "total_in", "structmz__stream__s.html#abfa083eb7707360c33389ec12fadf376", null ],
    [ "total_out", "structmz__stream__s.html#a5cf2c15cc49a99ee7d541375798a3e27", null ],
    [ "zalloc", "structmz__stream__s.html#abbd8109a7f88713f39b9e3046a223ef2", null ],
    [ "zfree", "structmz__stream__s.html#a55dbac0e9b86472bfa41f86dfb35df9a", null ]
];