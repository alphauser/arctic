var structarctic_1_1_sound_mixer_state =
[
    [ "AddSoundTask", "structarctic_1_1_sound_mixer_state.html#afeb3dab1907b6fca55bd35026812bf8c", null ],
    [ "InputTasksToMixerThread", "structarctic_1_1_sound_mixer_state.html#a8b086bc44218aa2d5e01863fae1a9f5e", null ],
    [ "buffers", "structarctic_1_1_sound_mixer_state.html#ae5cb6e41076c7cdbf6dc48c8c1c49c34", null ],
    [ "do_quit", "structarctic_1_1_sound_mixer_state.html#a254a26c7895c83f653d480a14d7fd00d", null ],
    [ "master_volume", "structarctic_1_1_sound_mixer_state.html#ac78bc23a6aeeb24e6dd26fc17fff7ac3", null ],
    [ "task_count", "structarctic_1_1_sound_mixer_state.html#ae7271973fb3a19d10ef527c4294a0750", null ],
    [ "task_mutex", "structarctic_1_1_sound_mixer_state.html#aef4df90904674471ec88c045ea5eb8f4", null ],
    [ "task_pushers", "structarctic_1_1_sound_mixer_state.html#a0fbbfc96f40cee3b18f6e08465f09a63", null ],
    [ "tasks", "structarctic_1_1_sound_mixer_state.html#a47a17b271ad4414d499a042d0e27146c", null ]
];