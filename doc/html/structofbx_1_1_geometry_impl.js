var structofbx_1_1_geometry_impl =
[
    [ "NewVertex", "structofbx_1_1_geometry_impl_1_1_new_vertex.html", "structofbx_1_1_geometry_impl_1_1_new_vertex" ],
    [ "VertexDataMapping", "structofbx_1_1_geometry_impl.html#ab81bd25139a1d629cec2592ecdd4479b", [
      [ "BY_POLYGON_VERTEX", "structofbx_1_1_geometry_impl.html#ab81bd25139a1d629cec2592ecdd4479ba6e7d4551f9abc9531646cc89e8fcb619", null ],
      [ "BY_POLYGON", "structofbx_1_1_geometry_impl.html#ab81bd25139a1d629cec2592ecdd4479bae7bc5b2fd19ae3bbd400c2d7e1333a87", null ],
      [ "BY_VERTEX", "structofbx_1_1_geometry_impl.html#ab81bd25139a1d629cec2592ecdd4479baa89484518f2e473f4770907fd67aab16", null ]
    ] ],
    [ "GeometryImpl", "structofbx_1_1_geometry_impl.html#a749d703738708eff989e8d2184f66f6f", null ],
    [ "getColors", "structofbx_1_1_geometry_impl.html#a5be12026b6059b7743836f28d4d66524", null ],
    [ "getMaterials", "structofbx_1_1_geometry_impl.html#adec450d97662fb3339d2227f0d3a4ace", null ],
    [ "getNormals", "structofbx_1_1_geometry_impl.html#ac09e52efa205f69e2643570b17c36ca8", null ],
    [ "getSkin", "structofbx_1_1_geometry_impl.html#a250745a2b516298a073a62ad5e112a1c", null ],
    [ "getTangents", "structofbx_1_1_geometry_impl.html#a6a4f0b11f21421d52e737cd940c95c79", null ],
    [ "getType", "structofbx_1_1_geometry_impl.html#aede58c8ff9b86e9eae0bf4156ca10eb9", null ],
    [ "getUVs", "structofbx_1_1_geometry_impl.html#a28cd7168fe85e82bbfaa0cce41152995", null ],
    [ "getVertexCount", "structofbx_1_1_geometry_impl.html#a85b146d09023c99ed6db1618ff070d33", null ],
    [ "getVertices", "structofbx_1_1_geometry_impl.html#aaa8be49bcbb12e09ddb30657b8526599", null ],
    [ "triangulate", "structofbx_1_1_geometry_impl.html#a96ae997cedef8544503da18d7e653093", null ],
    [ "colors", "structofbx_1_1_geometry_impl.html#ab9bffd3d12ff02e08d278d120c575829", null ],
    [ "materials", "structofbx_1_1_geometry_impl.html#a2f6f5831774c32f8d4f6baa62bfd5851", null ],
    [ "normals", "structofbx_1_1_geometry_impl.html#a60ff554b3ce2950dc41c1373c282ec2f", null ],
    [ "skin", "structofbx_1_1_geometry_impl.html#aa948ff73303d2405b252649baaa4694c", null ],
    [ "tangents", "structofbx_1_1_geometry_impl.html#aa401783ea37ce22213bd39b4346b083a", null ],
    [ "to_new_vertices", "structofbx_1_1_geometry_impl.html#a1a1fb2396f3fe363c86bf29a661b62d6", null ],
    [ "to_old_vertices", "structofbx_1_1_geometry_impl.html#a6a3335d54dc2612f7fdc40e2750cbd47", null ],
    [ "uvs", "structofbx_1_1_geometry_impl.html#ae0cdf377f5d5e60dacf99ba437fce38a", null ],
    [ "vertices", "structofbx_1_1_geometry_impl.html#a716dff4530d72430e52a7ac39943a373", null ]
];