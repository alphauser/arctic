var structarctic_1_1ofbx_1_1_global_settings =
[
    [ "CoordAxis", "structarctic_1_1ofbx_1_1_global_settings.html#a0a9d45df891657adc5c22e7f1ef2293b", null ],
    [ "CoordAxisSign", "structarctic_1_1ofbx_1_1_global_settings.html#a3d465a621f7f0e517fd52fca07759ecc", null ],
    [ "CustomFrameRate", "structarctic_1_1ofbx_1_1_global_settings.html#a165c49718b6031b7cc19831c1b437999", null ],
    [ "FrontAxis", "structarctic_1_1ofbx_1_1_global_settings.html#a9b6e848a63e1d4ddfe574e0c3761cc04", null ],
    [ "FrontAxisSign", "structarctic_1_1ofbx_1_1_global_settings.html#a6c3866146bde36307d73a97dea1df908", null ],
    [ "OriginalUnitScaleFactor", "structarctic_1_1ofbx_1_1_global_settings.html#a2b18e37a6c2a2b4e91363fbcb9eda6fd", null ],
    [ "OriginalUpAxis", "structarctic_1_1ofbx_1_1_global_settings.html#a3078526f7b7210e705a069d066e552a0", null ],
    [ "OriginalUpAxisSign", "structarctic_1_1ofbx_1_1_global_settings.html#af1dc62e1ffddbd5ba3ea6188cb0e34e0", null ],
    [ "TimeMode", "structarctic_1_1ofbx_1_1_global_settings.html#a119b4dcbbc4fbee7e5fecba63f950953", null ],
    [ "TimeSpanStart", "structarctic_1_1ofbx_1_1_global_settings.html#aa7c9fe2a225def70dd84a86c8c851218", null ],
    [ "TimeSpanStop", "structarctic_1_1ofbx_1_1_global_settings.html#aff0e59b97672e094261082534f5a77ab", null ],
    [ "UnitScaleFactor", "structarctic_1_1ofbx_1_1_global_settings.html#ae32cc6e5728ffcce16da4b72c51c1e3c", null ],
    [ "UpAxis", "structarctic_1_1ofbx_1_1_global_settings.html#a9dc4cbd1d7f182c7a83e4cf71fafec41", null ],
    [ "UpAxisSign", "structarctic_1_1ofbx_1_1_global_settings.html#adcb160d5521f4c5abc24664564d6e696", null ]
];