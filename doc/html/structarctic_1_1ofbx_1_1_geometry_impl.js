var structarctic_1_1ofbx_1_1_geometry_impl =
[
    [ "NewVertex", "structarctic_1_1ofbx_1_1_geometry_impl_1_1_new_vertex.html", "structarctic_1_1ofbx_1_1_geometry_impl_1_1_new_vertex" ],
    [ "VertexDataMapping", "structarctic_1_1ofbx_1_1_geometry_impl.html#ac9a8f9602f810418cebf73a23aabff2f", [
      [ "BY_POLYGON_VERTEX", "structarctic_1_1ofbx_1_1_geometry_impl.html#ac9a8f9602f810418cebf73a23aabff2faf42b0c0dee84bea99fbf07f7e696c006", null ],
      [ "BY_POLYGON", "structarctic_1_1ofbx_1_1_geometry_impl.html#ac9a8f9602f810418cebf73a23aabff2fa3e8a2433e788f7a5ce6adaad33cf7a8c", null ],
      [ "BY_VERTEX", "structarctic_1_1ofbx_1_1_geometry_impl.html#ac9a8f9602f810418cebf73a23aabff2fae9188f29d9606abca808a8863ea71aa8", null ]
    ] ],
    [ "GeometryImpl", "structarctic_1_1ofbx_1_1_geometry_impl.html#a95d68585e406f4bda916737a0b730340", null ],
    [ "getColors", "structarctic_1_1ofbx_1_1_geometry_impl.html#a98cb9a4e7f576b37a3e83a7189a9464b", null ],
    [ "getMaterials", "structarctic_1_1ofbx_1_1_geometry_impl.html#a6e824f79685ef6647823edb86fc8bde7", null ],
    [ "getNormals", "structarctic_1_1ofbx_1_1_geometry_impl.html#aad7cd5bf95cbd7ecd7922b381d17a9ab", null ],
    [ "getSkin", "structarctic_1_1ofbx_1_1_geometry_impl.html#aff463402b8135393f4f192fb4b951e5e", null ],
    [ "getTangents", "structarctic_1_1ofbx_1_1_geometry_impl.html#ad2b2d214c151b020b1c464120b2f9053", null ],
    [ "getType", "structarctic_1_1ofbx_1_1_geometry_impl.html#a5ebabd76faa3d25c95599c03469c1318", null ],
    [ "getUVs", "structarctic_1_1ofbx_1_1_geometry_impl.html#a14835cc6dd0e47fa606f449481aa4a01", null ],
    [ "getVertexCount", "structarctic_1_1ofbx_1_1_geometry_impl.html#a9e9fc65b12b8c1a1cf2489f2ea3d3236", null ],
    [ "getVertices", "structarctic_1_1ofbx_1_1_geometry_impl.html#a2b88d3a5878b3bd6312aa7fabd8c6978", null ],
    [ "triangulate", "structarctic_1_1ofbx_1_1_geometry_impl.html#a514e118e18ec2f786990f0ac4baf6f6a", null ],
    [ "colors", "structarctic_1_1ofbx_1_1_geometry_impl.html#a98d8745fe290d824b36c9914c536fba5", null ],
    [ "materials", "structarctic_1_1ofbx_1_1_geometry_impl.html#a6ccd00e79613ae456d1f2329469e60a7", null ],
    [ "normals", "structarctic_1_1ofbx_1_1_geometry_impl.html#a741bbcfb198bf930f9a464589573f6d1", null ],
    [ "skin", "structarctic_1_1ofbx_1_1_geometry_impl.html#abc98d5830e9fba700d9395e8f96fba6f", null ],
    [ "tangents", "structarctic_1_1ofbx_1_1_geometry_impl.html#a3ae5b429c13ce6d6ca64b48220cc236e", null ],
    [ "to_new_vertices", "structarctic_1_1ofbx_1_1_geometry_impl.html#a026ae6eb1d6ea091c0843ab4fbf3388e", null ],
    [ "to_old_vertices", "structarctic_1_1ofbx_1_1_geometry_impl.html#aeffc884065c958aeef9ea317858a4a34", null ],
    [ "uvs", "structarctic_1_1ofbx_1_1_geometry_impl.html#aed7d0407a7503adfa98ebbeb52d62c76", null ],
    [ "vertices", "structarctic_1_1ofbx_1_1_geometry_impl.html#a70bd3eb9b70765c88e99ec8daff8eeff", null ]
];