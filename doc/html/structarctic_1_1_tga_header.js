var structarctic_1_1_tga_header =
[
    [ "color_map_entry_size", "structarctic_1_1_tga_header.html#a98a59b29201e37bef056ff2119e1cc0d", null ],
    [ "color_map_length", "structarctic_1_1_tga_header.html#a5d8e63da2b0ef3a372c82ed40dd17c4e", null ],
    [ "color_map_origin", "structarctic_1_1_tga_header.html#aa329c26b2752b7545dc569b3e02157f3", null ],
    [ "color_map_type", "structarctic_1_1_tga_header.html#a9cf20bdacdfc585dd88e94e3d7fee17a", null ],
    [ "id_field_length", "structarctic_1_1_tga_header.html#ae94a4f1ce5052714436a586bfc74371f", null ],
    [ "image_descriptor", "structarctic_1_1_tga_header.html#ac85a76513598b3bbd2ae6b02d406053e", null ],
    [ "image_height", "structarctic_1_1_tga_header.html#a318e47b461a011b3ef507b6bac91024b", null ],
    [ "image_type", "structarctic_1_1_tga_header.html#a2aa7423b5cdd7577a5257ede2275a07a", null ],
    [ "image_width", "structarctic_1_1_tga_header.html#af76ce241bd69bc62606ef8593ca95cb8", null ],
    [ "image_x_origin", "structarctic_1_1_tga_header.html#a35cbaa5f586793308d074282d9f5118f", null ],
    [ "image_y_origin", "structarctic_1_1_tga_header.html#a7db24434ac5f6d4f932e90d2fd970de6", null ],
    [ "pixel_depth", "structarctic_1_1_tga_header.html#abbccbf908f8884edeb8f73d056d655d1", null ]
];