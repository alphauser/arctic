var hierarchy =
[
    [ "arctic::AudioDeviceInfo", "classarctic_1_1_audio_device_info.html", null ],
    [ "arctic::BmFontBinChars", "structarctic_1_1_bm_font_bin_chars.html", null ],
    [ "arctic::BmFontBinCommon", "structarctic_1_1_bm_font_bin_common.html", null ],
    [ "arctic::BmFontBinHeader", "structarctic_1_1_bm_font_bin_header.html", null ],
    [ "arctic::BmFontBinInfo", "structarctic_1_1_bm_font_bin_info.html", null ],
    [ "arctic::BmFontBinKerningPair", "structarctic_1_1_bm_font_bin_kerning_pair.html", null ],
    [ "arctic::BmFontBinPages", "structarctic_1_1_bm_font_bin_pages.html", null ],
    [ "arctic::Bound2F", "structarctic_1_1_bound2_f.html", null ],
    [ "arctic::Bound3F", "structarctic_1_1_bound3_f.html", null ],
    [ "arctic::ofbx::Color", "structarctic_1_1ofbx_1_1_color.html", null ],
    [ "arctic::ofbx::Scene::Connection", "structarctic_1_1ofbx_1_1_scene_1_1_connection.html", null ],
    [ "arctic::CsvRow", "classarctic_1_1_csv_row.html", null ],
    [ "arctic::CsvTable", "classarctic_1_1_csv_table.html", null ],
    [ "arctic::ofbx::Cursor", "structarctic_1_1ofbx_1_1_cursor.html", null ],
    [ "arctic::ofbx::AnimationCurveNodeImpl::Curve", "structarctic_1_1ofbx_1_1_animation_curve_node_impl_1_1_curve.html", null ],
    [ "arctic::ofbx::DataView", "structarctic_1_1ofbx_1_1_data_view.html", null ],
    [ "arctic::DirectoryEntry", "structarctic_1_1_directory_entry.html", null ],
    [ "enable_shared_from_this", null, [
      [ "arctic::Panel", "classarctic_1_1_panel.html", [
        [ "arctic::Button", "classarctic_1_1_button.html", null ],
        [ "arctic::Editbox", "classarctic_1_1_editbox.html", null ],
        [ "arctic::HorizontalScroll", "classarctic_1_1_horizontal_scroll.html", null ],
        [ "arctic::Progressbar", "classarctic_1_1_progressbar.html", null ],
        [ "arctic::Text", "classarctic_1_1_text.html", null ]
      ] ]
    ] ],
    [ "arctic::Engine", "classarctic_1_1_engine.html", null ],
    [ "arctic::ofbx::Error", "structarctic_1_1ofbx_1_1_error.html", null ],
    [ "arctic::Font", "structarctic_1_1_font.html", null ],
    [ "arctic::Frustum3F", "structarctic_1_1_frustum3_f.html", null ],
    [ "arctic::ofbx::GlobalSettings", "structarctic_1_1ofbx_1_1_global_settings.html", null ],
    [ "arctic::Glyph", "structarctic_1_1_glyph.html", null ],
    [ "arctic::GuiMessage", "classarctic_1_1_gui_message.html", null ],
    [ "arctic::ofbx::Header", "structarctic_1_1ofbx_1_1_header.html", null ],
    [ "arctic::ofbx::IElement", "structarctic_1_1ofbx_1_1_i_element.html", [
      [ "arctic::ofbx::Element", "structarctic_1_1ofbx_1_1_element.html", null ]
    ] ],
    [ "arctic::ofbx::IElementProperty", "structarctic_1_1ofbx_1_1_i_element_property.html", [
      [ "arctic::ofbx::Property", "structarctic_1_1ofbx_1_1_property.html", null ]
    ] ],
    [ "inflate_state", "structinflate__state.html", null ],
    [ "arctic::InputMessage", "structarctic_1_1_input_message.html", null ],
    [ "arctic::ofbx::IScene", "structarctic_1_1ofbx_1_1_i_scene.html", [
      [ "arctic::ofbx::Scene", "structarctic_1_1ofbx_1_1_scene.html", null ]
    ] ],
    [ "arctic::InputMessage::Keyboard", "structarctic_1_1_input_message_1_1_keyboard.html", null ],
    [ "arctic::easy::KeyState", "structarctic_1_1easy_1_1_key_state.html", null ],
    [ "arctic::Mat22F", "structarctic_1_1_mat22_f.html", null ],
    [ "arctic::Mat33F", "structarctic_1_1_mat33_f.html", null ],
    [ "arctic::Mat44F", "structarctic_1_1_mat44_f.html", null ],
    [ "arctic::MathTables", "structarctic_1_1_math_tables.html", null ],
    [ "arctic::ofbx::Matrix", "structarctic_1_1ofbx_1_1_matrix.html", null ],
    [ "arctic::InputMessage::Mouse", "structarctic_1_1_input_message_1_1_mouse.html", null ],
    [ "mz_stream_s", "structmz__stream__s.html", null ],
    [ "mz_zip_archive", "structmz__zip__archive.html", null ],
    [ "mz_zip_archive_file_stat", "structmz__zip__archive__file__stat.html", null ],
    [ "mz_zip_array", "structmz__zip__array.html", null ],
    [ "mz_zip_internal_state_tag", "structmz__zip__internal__state__tag.html", null ],
    [ "mz_zip_reader_extract_iter_state", "structmz__zip__reader__extract__iter__state.html", null ],
    [ "mz_zip_writer_add_state", "structmz__zip__writer__add__state.html", null ],
    [ "arctic::ofbx::GeometryImpl::NewVertex", "structarctic_1_1ofbx_1_1_geometry_impl_1_1_new_vertex.html", null ],
    [ "arctic::ofbx::Object", "structarctic_1_1ofbx_1_1_object.html", [
      [ "arctic::ofbx::AnimationCurve", "structarctic_1_1ofbx_1_1_animation_curve.html", [
        [ "arctic::ofbx::AnimationCurveImpl", "structarctic_1_1ofbx_1_1_animation_curve_impl.html", null ]
      ] ],
      [ "arctic::ofbx::AnimationCurveNode", "structarctic_1_1ofbx_1_1_animation_curve_node.html", [
        [ "arctic::ofbx::AnimationCurveNodeImpl", "structarctic_1_1ofbx_1_1_animation_curve_node_impl.html", null ]
      ] ],
      [ "arctic::ofbx::AnimationLayer", "structarctic_1_1ofbx_1_1_animation_layer.html", [
        [ "arctic::ofbx::AnimationLayerImpl", "structarctic_1_1ofbx_1_1_animation_layer_impl.html", null ]
      ] ],
      [ "arctic::ofbx::AnimationStack", "structarctic_1_1ofbx_1_1_animation_stack.html", [
        [ "arctic::ofbx::AnimationStackImpl", "structarctic_1_1ofbx_1_1_animation_stack_impl.html", null ]
      ] ],
      [ "arctic::ofbx::Cluster", "structarctic_1_1ofbx_1_1_cluster.html", [
        [ "arctic::ofbx::ClusterImpl", "structarctic_1_1ofbx_1_1_cluster_impl.html", null ]
      ] ],
      [ "arctic::ofbx::Geometry", "structarctic_1_1ofbx_1_1_geometry.html", [
        [ "arctic::ofbx::GeometryImpl", "structarctic_1_1ofbx_1_1_geometry_impl.html", null ]
      ] ],
      [ "arctic::ofbx::LimbNodeImpl", "structarctic_1_1ofbx_1_1_limb_node_impl.html", null ],
      [ "arctic::ofbx::Material", "structarctic_1_1ofbx_1_1_material.html", [
        [ "arctic::ofbx::MaterialImpl", "structarctic_1_1ofbx_1_1_material_impl.html", null ]
      ] ],
      [ "arctic::ofbx::Mesh", "structarctic_1_1ofbx_1_1_mesh.html", [
        [ "arctic::ofbx::MeshImpl", "structarctic_1_1ofbx_1_1_mesh_impl.html", null ]
      ] ],
      [ "arctic::ofbx::NodeAttribute", "structarctic_1_1ofbx_1_1_node_attribute.html", [
        [ "arctic::ofbx::NodeAttributeImpl", "structarctic_1_1ofbx_1_1_node_attribute_impl.html", null ]
      ] ],
      [ "arctic::ofbx::NullImpl", "structarctic_1_1ofbx_1_1_null_impl.html", null ],
      [ "arctic::ofbx::Root", "structarctic_1_1ofbx_1_1_root.html", null ],
      [ "arctic::ofbx::Skin", "structarctic_1_1ofbx_1_1_skin.html", [
        [ "arctic::ofbx::SkinImpl", "structarctic_1_1ofbx_1_1_skin_impl.html", null ]
      ] ],
      [ "arctic::ofbx::Texture", "structarctic_1_1ofbx_1_1_texture.html", [
        [ "arctic::ofbx::TextureImpl", "structarctic_1_1ofbx_1_1_texture_impl.html", null ]
      ] ]
    ] ],
    [ "arctic::ofbx::Scene::ObjectPair", "structarctic_1_1ofbx_1_1_scene_1_1_object_pair.html", null ],
    [ "arctic::ofbx::OptionalError< T >", "structarctic_1_1ofbx_1_1_optional_error.html", null ],
    [ "arctic::ofbx::Quat", "structarctic_1_1ofbx_1_1_quat.html", null ],
    [ "arctic::Rgb", "structarctic_1_1_rgb.html", null ],
    [ "arctic::Rgba", "structarctic_1_1_rgba.html", null ],
    [ "arctic::easy::Sound", "classarctic_1_1easy_1_1_sound.html", null ],
    [ "arctic::SoundBuffer", "structarctic_1_1_sound_buffer.html", null ],
    [ "arctic::easy::SoundInstance", "classarctic_1_1easy_1_1_sound_instance.html", null ],
    [ "arctic::SoundMixerState", "structarctic_1_1_sound_mixer_state.html", null ],
    [ "arctic::SoundPlayer", "classarctic_1_1_sound_player.html", null ],
    [ "arctic::easy::SpanSi32", "structarctic_1_1easy_1_1_span_si32.html", null ],
    [ "arctic::easy::Sprite", "classarctic_1_1easy_1_1_sprite.html", null ],
    [ "arctic::easy::SpriteInstance", "classarctic_1_1easy_1_1_sprite_instance.html", null ],
    [ "arctic::ofbx::TakeInfo", "structarctic_1_1ofbx_1_1_take_info.html", null ],
    [ "tdefl_compressor", "structtdefl__compressor.html", null ],
    [ "tdefl_output_buffer", "structtdefl__output__buffer.html", null ],
    [ "tdefl_sym_freq", "structtdefl__sym__freq.html", null ],
    [ "test__", "structtest____.html", null ],
    [ "arctic::TgaHeader", "structarctic_1_1_tga_header.html", null ],
    [ "tinfl_decompressor_tag", "structtinfl__decompressor__tag.html", null ],
    [ "tinfl_huff_table", "structtinfl__huff__table.html", null ],
    [ "arctic::Utf32Reader", "structarctic_1_1_utf32_reader.html", null ],
    [ "arctic::ofbx::Vec2", "structarctic_1_1ofbx_1_1_vec2.html", null ],
    [ "arctic::Vec2F", "structarctic_1_1_vec2_f.html", null ],
    [ "arctic::Vec2Si32", "structarctic_1_1_vec2_si32.html", null ],
    [ "arctic::ofbx::Vec3", "structarctic_1_1ofbx_1_1_vec3.html", null ],
    [ "arctic::Vec3F", "structarctic_1_1_vec3_f.html", null ],
    [ "arctic::Vec3Si32", "structarctic_1_1_vec3_si32.html", null ],
    [ "arctic::ofbx::Vec4", "structarctic_1_1ofbx_1_1_vec4.html", null ],
    [ "arctic::Vec4F", "structarctic_1_1_vec4_f.html", null ],
    [ "arctic::Vec4Si32", "structarctic_1_1_vec4_si32.html", null ],
    [ "arctic::easy::WaveHeader", "structarctic_1_1easy_1_1_wave_header.html", null ]
];