var structarctic_1_1_bm_font_bin_info =
[
    [ "Bits", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41", [
      [ "kSmooth", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41a01b262cb51f568cac8335d8d66fd3559", null ],
      [ "kUnicode", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41a0fd1862dfad2c264df6d97135727cbb0", null ],
      [ "kItalic", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41a1bf67c296815a073aad2d28fa0f60ba2", null ],
      [ "kBold", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41a38ae051c51ec7051a58fcef078992222", null ],
      [ "kFixedHeight", "structarctic_1_1_bm_font_bin_info.html#ae97951b5ee4700301952c17a31406c41a3427b02169602a44770967ea8de05980", null ]
    ] ],
    [ "Log", "structarctic_1_1_bm_font_bin_info.html#a51909538f19718d01e47d6602df6672c", null ],
    [ "aa", "structarctic_1_1_bm_font_bin_info.html#a7af0213aa1eaa933e851743cb6578217", null ],
    [ "bits", "structarctic_1_1_bm_font_bin_info.html#ad9f39f955cf8c6c153ed276b884cc4a9", null ],
    [ "char_set", "structarctic_1_1_bm_font_bin_info.html#aaff754df4926d3e246d786ba5d8623d3", null ],
    [ "font_name", "structarctic_1_1_bm_font_bin_info.html#ac8862d7abdc3c11d91fc659baced6683", null ],
    [ "font_size", "structarctic_1_1_bm_font_bin_info.html#a83048da8c2dfc12342dfdb8966335b44", null ],
    [ "outline", "structarctic_1_1_bm_font_bin_info.html#a19ce549271977484a7fe32c3506d0445", null ],
    [ "padding_down", "structarctic_1_1_bm_font_bin_info.html#aa05960b8f08b6c93654cbf02ef0a78ac", null ],
    [ "padding_left", "structarctic_1_1_bm_font_bin_info.html#a7c7aa6a4b215eef12f34896fd354d0a6", null ],
    [ "padding_right", "structarctic_1_1_bm_font_bin_info.html#ad885b46f510c148a87a93df54b2f4398", null ],
    [ "padding_up", "structarctic_1_1_bm_font_bin_info.html#a245ca41d2fac7a13c6c4877c0bfdcb4c", null ],
    [ "spacing_horiz", "structarctic_1_1_bm_font_bin_info.html#af295637d2cd003770c495067a56f0e74", null ],
    [ "spacing_vert", "structarctic_1_1_bm_font_bin_info.html#aac96eb8390ba4d9ceacab01af531a5b6", null ],
    [ "stretch_h", "structarctic_1_1_bm_font_bin_info.html#a12521467fe31fd593a8c8ed38cee43bc", null ]
];