/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Arctic Engine", "index.html", [
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classarctic_1_1easy_1_1_sprite.html#ac21d6b8e3fef34ac187f4f4991c09348",
"structarctic_1_1_sound_buffer.html#a707e39f36c88c7834b8c58bc1d772fd3ae53fbac2bb7cf833b3e5aaa9233fa99d",
"structarctic_1_1_vec4_f.html#ac58109e49907a52d88414abecea1c933",
"structarctic_1_1ofbx_1_1_geometry_impl.html#a98cb9a4e7f576b37a3e83a7189a9464b",
"structarctic_1_1ofbx_1_1_texture.html#a796d2e69fd9c7e34802090c07d5cfce6a3096490da2c1ebfb09a17c6f56c8d28e"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';