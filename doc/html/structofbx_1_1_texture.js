var structofbx_1_1_texture =
[
    [ "TextureType", "structofbx_1_1_texture.html#a69ce52c37fa759a784c983c13b35a928", [
      [ "DIFFUSE", "structofbx_1_1_texture.html#a69ce52c37fa759a784c983c13b35a928aba30c013702555e1977488954379ae04", null ],
      [ "NORMAL", "structofbx_1_1_texture.html#a69ce52c37fa759a784c983c13b35a928a0717cc4e0c96202ba10f54c351685a2f", null ],
      [ "COUNT", "structofbx_1_1_texture.html#a69ce52c37fa759a784c983c13b35a928ad2d9463e873ce321339004717419de7c", null ]
    ] ],
    [ "Texture", "structofbx_1_1_texture.html#a001f45009780e37c2ca3b8a8e61711dc", null ],
    [ "getFileName", "structofbx_1_1_texture.html#a129c52e90db37d3bf79ef22a156dbec3", null ],
    [ "getRelativeFileName", "structofbx_1_1_texture.html#a58d04ddf17045ec958d45756baaeaa64", null ]
];