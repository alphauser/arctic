var structofbx_1_1_texture_impl =
[
    [ "TextureImpl", "structofbx_1_1_texture_impl.html#aac30e9be252413d79bb4be5f29e6f175", null ],
    [ "getFileName", "structofbx_1_1_texture_impl.html#a44fa3ce67258d5bb2d55a9b2b94da418", null ],
    [ "getRelativeFileName", "structofbx_1_1_texture_impl.html#a2e3d819f1b779c2fc9e80c50d0361f94", null ],
    [ "getType", "structofbx_1_1_texture_impl.html#a89386eea337a7e902254ba1cf6d5dce0", null ],
    [ "filename", "structofbx_1_1_texture_impl.html#abfe7f7b8fb8fb5684bb4116d4a051886", null ],
    [ "relative_filename", "structofbx_1_1_texture_impl.html#a662fd116c66ac83ebd222d6996b76ba7", null ]
];