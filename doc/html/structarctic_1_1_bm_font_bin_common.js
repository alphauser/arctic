var structarctic_1_1_bm_font_bin_common =
[
    [ "Bits", "structarctic_1_1_bm_font_bin_common.html#a4726a9f5ec0ea4c0feac11de1a6c3d83", [
      [ "kPacked", "structarctic_1_1_bm_font_bin_common.html#a4726a9f5ec0ea4c0feac11de1a6c3d83ae1d83fb54cacd3959e1a88c77f0ae716", null ]
    ] ],
    [ "Log", "structarctic_1_1_bm_font_bin_common.html#a0ee807b7e03151c6c75ef7b030020789", null ],
    [ "alpha_chnl", "structarctic_1_1_bm_font_bin_common.html#a8920a85958519c2427e446aba11f377e", null ],
    [ "base", "structarctic_1_1_bm_font_bin_common.html#a2e4487713aa60f3699afb68ab7fcd67b", null ],
    [ "bits", "structarctic_1_1_bm_font_bin_common.html#a439effa05c36a493d7e39d231a416d18", null ],
    [ "blue_chnl", "structarctic_1_1_bm_font_bin_common.html#a136d5b2b0838afd75ff9000bae09a84e", null ],
    [ "green_chnl", "structarctic_1_1_bm_font_bin_common.html#a9c08df2a3226bc66607fa7dca4d32408", null ],
    [ "line_height", "structarctic_1_1_bm_font_bin_common.html#a0573eb6146a59581a8f471d211feeb87", null ],
    [ "pages", "structarctic_1_1_bm_font_bin_common.html#a7bfe6ca6ac82a8ed624c054d06767035", null ],
    [ "red_chnl", "structarctic_1_1_bm_font_bin_common.html#ada6954dd78ea879515280c2b957e76ed", null ],
    [ "scale_h", "structarctic_1_1_bm_font_bin_common.html#a9839c4f2f8d083e41dec95da94506ca1", null ],
    [ "scale_w", "structarctic_1_1_bm_font_bin_common.html#a2baab415351146be612d12ea86ae6fe3", null ]
];