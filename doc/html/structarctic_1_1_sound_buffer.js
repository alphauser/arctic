var structarctic_1_1_sound_buffer =
[
    [ "Action", "structarctic_1_1_sound_buffer.html#a707e39f36c88c7834b8c58bc1d772fd3", [
      [ "kStart", "structarctic_1_1_sound_buffer.html#a707e39f36c88c7834b8c58bc1d772fd3a91623fda47157b927b0858bb8d78cf70", null ],
      [ "kStop", "structarctic_1_1_sound_buffer.html#a707e39f36c88c7834b8c58bc1d772fd3ae53fbac2bb7cf833b3e5aaa9233fa99d", null ]
    ] ],
    [ "action", "structarctic_1_1_sound_buffer.html#a7d4ccf8ced52ace1437b1707a75af7ca", null ],
    [ "next_position", "structarctic_1_1_sound_buffer.html#a0e22db4786c465a607fd0cb2a8a6a3a5", null ],
    [ "sound", "structarctic_1_1_sound_buffer.html#a645b3d79acdbcfd07ff1a7464d0a01f2", null ],
    [ "volume", "structarctic_1_1_sound_buffer.html#a25df3907be828c5492b8139a2011efd1", null ]
];