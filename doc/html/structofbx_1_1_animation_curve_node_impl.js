var structofbx_1_1_animation_curve_node_impl =
[
    [ "Curve", "structofbx_1_1_animation_curve_node_impl_1_1_curve.html", "structofbx_1_1_animation_curve_node_impl_1_1_curve" ],
    [ "Mode", "structofbx_1_1_animation_curve_node_impl.html#aad381d25136f9b6a19c9e9a59408829c", [
      [ "TRANSLATION", "structofbx_1_1_animation_curve_node_impl.html#aad381d25136f9b6a19c9e9a59408829ca47f5b4c13abb4a8532cef0388022f560", null ],
      [ "ROTATION", "structofbx_1_1_animation_curve_node_impl.html#aad381d25136f9b6a19c9e9a59408829ca328719d3839e4b8128871d59557454d6", null ],
      [ "SCALE", "structofbx_1_1_animation_curve_node_impl.html#aad381d25136f9b6a19c9e9a59408829cad078e8a21a645c512d32ed87d4c6d4d6", null ]
    ] ],
    [ "AnimationCurveNodeImpl", "structofbx_1_1_animation_curve_node_impl.html#aa1607da4b63cef05399318c94d45b897", null ],
    [ "getBone", "structofbx_1_1_animation_curve_node_impl.html#afe187a968f938f3636a3ba6033e3a0b1", null ],
    [ "getNodeLocalTransform", "structofbx_1_1_animation_curve_node_impl.html#adaf31abcde31f42df067026095c3c815", null ],
    [ "getType", "structofbx_1_1_animation_curve_node_impl.html#aba9ef4194ea2e4dcf851916b45747e37", null ],
    [ "bone", "structofbx_1_1_animation_curve_node_impl.html#ac80e3c597f01ec049f978eef7340e289", null ],
    [ "bone_link_property", "structofbx_1_1_animation_curve_node_impl.html#ac005bfbd8373cfab45bceca961079929", null ],
    [ "curves", "structofbx_1_1_animation_curve_node_impl.html#af85f743d875a5d9e5551554b9e09e9bf", null ],
    [ "mode", "structofbx_1_1_animation_curve_node_impl.html#a23afeda470469bb915dfcbbdceb7c291", null ]
];