var structofbx_1_1_global_settings =
[
    [ "CoordAxis", "structofbx_1_1_global_settings.html#afc11476952a3acdf6f54e03e5b80bef0", null ],
    [ "CoordAxisSign", "structofbx_1_1_global_settings.html#af81282a263b0d399c64057f12b55d93e", null ],
    [ "CustomFrameRate", "structofbx_1_1_global_settings.html#a5da9545ba019f0629efa259b94d2a307", null ],
    [ "FrontAxis", "structofbx_1_1_global_settings.html#a57c751ca01935a45053af0413dbfbf08", null ],
    [ "FrontAxisSign", "structofbx_1_1_global_settings.html#a5d60c77493c943217649711cf00769a3", null ],
    [ "OriginalUnitScaleFactor", "structofbx_1_1_global_settings.html#a4b5d856e39df296388d599f65795da39", null ],
    [ "OriginalUpAxis", "structofbx_1_1_global_settings.html#a297e31354fa0be129f7b2bf278fa99ef", null ],
    [ "OriginalUpAxisSign", "structofbx_1_1_global_settings.html#aa66ec56339e321fd8e86a960e705b076", null ],
    [ "TimeMode", "structofbx_1_1_global_settings.html#a982c2a5460ad4382dd212dfb2673d901", null ],
    [ "TimeSpanStart", "structofbx_1_1_global_settings.html#a1acc0eea668d497024adf67589ba3d33", null ],
    [ "TimeSpanStop", "structofbx_1_1_global_settings.html#ae7584ff4d25fc4f9ee532543fe9728ba", null ],
    [ "UnitScaleFactor", "structofbx_1_1_global_settings.html#a3e3e5725e4ff553a9d742d1d648e7e5d", null ],
    [ "UpAxis", "structofbx_1_1_global_settings.html#ad9a407e591e6f2d262f83cc6e272b7e9", null ],
    [ "UpAxisSign", "structofbx_1_1_global_settings.html#a2da0a98794308fe4778e815460ab99a3", null ]
];