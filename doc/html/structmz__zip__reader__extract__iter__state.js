var structmz__zip__reader__extract__iter__state =
[
    [ "comp_remaining", "structmz__zip__reader__extract__iter__state.html#a835bf9de4597638b9ae3ef3e8f7f770a", null ],
    [ "cur_file_ofs", "structmz__zip__reader__extract__iter__state.html#aee3911ccbf924ff0d716720809f7fa8a", null ],
    [ "file_crc32", "structmz__zip__reader__extract__iter__state.html#a92097e7958c03a4f7c4f9f5f9145269d", null ],
    [ "file_stat", "structmz__zip__reader__extract__iter__state.html#a70e07c7c822ee9014f5092be0b3c01ff", null ],
    [ "flags", "structmz__zip__reader__extract__iter__state.html#a642c34a994d0e0d609e352d8ac570949", null ],
    [ "inflator", "structmz__zip__reader__extract__iter__state.html#a67b74d6a64f6672299ca23dc33055746", null ],
    [ "out_blk_remain", "structmz__zip__reader__extract__iter__state.html#ae2d3b4cb248278c1da0f8a613e97649b", null ],
    [ "out_buf_ofs", "structmz__zip__reader__extract__iter__state.html#a4cfc70cb49cd1edc40bc9c39c1525296", null ],
    [ "pRead_buf", "structmz__zip__reader__extract__iter__state.html#af147b2efe9759c947b25c765c0370d46", null ],
    [ "pWrite_buf", "structmz__zip__reader__extract__iter__state.html#a5a829046383b4865e42f696af6fb2985", null ],
    [ "pZip", "structmz__zip__reader__extract__iter__state.html#abbefe0a8d7cd48f3d2f4316db8d5760b", null ],
    [ "read_buf_avail", "structmz__zip__reader__extract__iter__state.html#aa1f8c854643105032293013ef1f63e99", null ],
    [ "read_buf_ofs", "structmz__zip__reader__extract__iter__state.html#acf293fcde5b8047049770936f480ef35", null ],
    [ "read_buf_size", "structmz__zip__reader__extract__iter__state.html#a5d1660fbffb6e64fcf7fc92c71e75b93", null ],
    [ "status", "structmz__zip__reader__extract__iter__state.html#a19a8d06ab3128434e7e7cd1665dfddcc", null ]
];